'use strict';
/*globals _,moment */
// Report controller
angular.module('reports').controller('ReportsController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Users', 'Projects',
	function($scope, $stateParams, $location, $http, Authentication, Users, Projects) {
		$scope.authentication = Authentication;
		$scope.projects = Projects.getList();
		$scope.report = {};
		

		/* **********************************
		* converts object in to proper format.
		* group by user, then project, then status
		* after that merge daily into one - sort by date.
		* ************************************/
		var generateTsReport = function(data,start,end){
			var statuses = [];
			start = start?moment(start).format('YYYY-MM-DD'):'0';
			end = end?moment(end).format('YYYY-MM-DD'):'9';
			var compare = function (a, b) {
				if (a.date > b.date) { return 1;}
				if (a.date < b.date) {return -1;}
				return 0;
			};

			data = _.groupBy(data,function(obj){return obj.user + '|' +obj.project;});
			_.forEach(data, function(obj , key){
				data[key] = _.groupBy(obj,function(obj){return obj.status;});
				var tsData = data[key];
				var total = 0;
				_.forEach(tsData,function(obj,key){
					statuses.push(key);
					tsData[key] = {};
					tsData[key].daily = _.flatten(_.map(obj,'daily')).sort(compare);
					_.remove(tsData[key].daily, function(day){
						if(day.date < start || day.date > end){ return true;}
					});
					tsData[key].hours = _.reduce(tsData[key].daily, function(result, day) {
						return result+ (day.hours?day.hours:0);
					}, 0);
					tsData[key].docs = _.flatten(_.map(obj,'docs'));
					total += tsData[key].hours;
				});
				data[key].hours = total;
				var tmp = key.split('|');
				data[key].user = _.find($scope.users, { _id: tmp[0] });
				data[key].project = _.find($scope.projects, { _id: tmp[1] });
			});
			/* //OLD LOGIC where grouped by user and project saperatley
			data = _.groupBy(data, function(obj) { return obj.user; });
			_.forEach(data, function(obj , key){
				data[key] = {projects:_.groupBy(obj,function(ts){return ts.project})};
				var projects = data[key].projects;
				_.forEach(projects, function(obj,key){
					projects[key] = {timesheets:_.groupBy(obj,function(ts){return ts.status})};
					var ts = projects[key].timesheets;
					_.forEach(ts,function(obj,key){
						//Adding various status
						statuses.push(key);
						ts[key] =   {daily: _.flatten(_.map(obj,'daily')).sort(compare)};
						//Remove Unwanted Daily prior date next date
						_.remove(ts[key].daily, function(day){
							if(day.date < start || day.date > end){ return true;}
						});
						ts[key].hours = _.reduce(ts[key].daily, function(result, day) {
							return result+ (day.hours?day.hours:0);
						}, 0);
					});
				});
			});*/
			$scope.tsReport = data;
			$scope.statuses = _.uniq(statuses);
		};
		$scope.downloadAll = function(row){
			console.log(row);
		};
		$scope.timeSheetReport = function(){
			delete $scope.timesheets;
			$http.post('/reports/timesheets', $scope.report).
				success(function(data, status, headers, config) {
					generateTsReport(data,$scope.report.startDate,$scope.report.endDate);
				}).
				error(function(data, status, headers, config) {
					alert('Error while getting report');
				});
		};
	}
]);
