/* global _*/
'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$rootScope',
	function($rootScope) {
		var _this = this;

		_this._data = {
			user: window.config.user
		};
		if(_.indexOf(_this._data.user.roles, 'admin')!==-1){
			$rootScope.isAdmin = true;
		}
		return _this._data;
	}
]);