/*globals _*/
'use strict';
angular.module('core')
.constant('globalLists',{
        timesheetCycles:['Weekly','Semi-Monthly'],
		invoiceCycles:['Weekly','Bi-Weekly','Semi-Monthly', 'Monthly'],
        days:['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
        contactTypes:['Primary', 'Work', 'Personal']
});



/*
.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});*/