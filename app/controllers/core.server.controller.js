'use strict';

/**
 * Module dependencies.
 */

var prependZero = function(num){
	if(num<10){
		num = '0'+num;
	}
	return num;
};

exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req,
		date: req._startTime.getFullYear() + '-' + prependZero(req._startTime.getMonth()+1) + '-' + prependZero(req._startTime.getDate())
	});
};